package cz.cvut.kbss.ear.model;

import java.util.Date;

public record Message(int id, String text, Date timeStamp, Chatter chatter) {}