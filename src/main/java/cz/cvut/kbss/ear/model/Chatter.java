package cz.cvut.kbss.ear.model;

public record Chatter (int id, String nickName) {}
