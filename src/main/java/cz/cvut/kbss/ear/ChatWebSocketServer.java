package cz.cvut.kbss.ear;

import cz.cvut.kbss.ear.model.Chatter;
import cz.cvut.kbss.ear.model.Message;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.spi.JsonProvider;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class ChatWebSocketServer extends TextWebSocketHandler {

  private static final Logger LOG = Logger.getLogger(ChatWebSocketServer.class.getName());

  private int chatterCount = 0;
  private int messageCount = 0;

  private final Set<WebSocketSession> sessions = new HashSet<>();
  private final Map<Integer, Chatter> chatters = new HashMap<>();
  private final Set<Message> messages = new HashSet<>();

  private void addMessage(Message msg) {
    messages.add(msg);
    JsonObject addMessage = createAddMessage(msg);
    sendToAllConnectedSessions(addMessage);
  }

  private void addChatter(Chatter chatter, WebSocketSession session) {
    chatters.put(chatter.id(), chatter);
    JsonObject addMessage = createAddChatter(chatter);
    sendToAllConnectedSessions(addMessage);

    JsonObject setChatter = createSetChatter(chatter);
    sendToSession(session, setChatter);
  }

  public void removeChatter(int id) {
    if (chatters.containsKey(id)) {
      chatters.remove(id);
      JsonProvider provider = JsonProvider.provider();
      JsonObject removeMessage = provider.createObjectBuilder()
          .add("action", "removeChatter")
          .add("id", id)
          .build();
      sendToAllConnectedSessions(removeMessage);
    }
  }

  private JsonObject createSetChatter(Chatter chatter) {
    return JsonProvider.provider().createObjectBuilder()
        .add("action", "setChatterId")
        .add("id", chatter.id())
        .build();
  }

  private JsonObject createAddChatter(Chatter chatter) {
    return JsonProvider.provider().createObjectBuilder()
        .add("action", "addChatter")
        .add("id", chatter.id())
        .add("nickName", chatter.nickName())
        .build();
  }

  private JsonObject createAddMessage(Message msg) {
    JsonProvider provider = JsonProvider.provider();
    return provider.createObjectBuilder()
        .add("action", "addMessage")
        .add("id", msg.id())
        .add("text", msg.text())
        .add("timestamp", new SimpleDateFormat().format(msg.timeStamp()))
        .add("chatterNickName", msg.chatter().nickName())
        .build();
  }

  private void sendToAllConnectedSessions(JsonObject message) {
    for (WebSocketSession session : sessions) {
      sendToSession(session, message);
    }
  }

  private void sendToSession(WebSocketSession session, JsonObject message) {
    LOG.log(Level.INFO, "Sending to {0} the message {1}", new Object[]{session, message});
    try {
      session.sendMessage(new TextMessage(message.toString()));
    } catch (IOException ex) {
      sessions.remove(session);
      LOG.log(Level.SEVERE, null, ex);
    }
  }

  //  Jakarta EE @OnOpen
  public void afterConnectionEstablished(WebSocketSession session) {
    sessions.add(session);
    for (Chatter chatter : chatters.values()) {
      JsonObject addMessage = createAddChatter(chatter);
      sendToSession(session, addMessage);
    }
    for (Message message : messages) {
      JsonObject addMessage = createAddMessage(message);
      sendToSession(session, addMessage);
    }
  }

  //  Jakarta EE @OnClose
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
    sessions.remove(session);
  }

  //  Jakarta EE @OnMessage
  public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) {
    try (JsonReader reader = Json.createReader(new StringReader(message.getPayload().toString()))) {
      JsonObject jsonMessage = reader.readObject();

      switch (jsonMessage.getString("action")) {
        case "addChatter":
          LOG.log(Level.INFO, "Adding chatter: {0}", message);
          addChatter(new Chatter(
              chatterCount++,
              jsonMessage.getString("nickName")
          ), session);
          break;
        case "removeChatter":
          LOG.log(Level.INFO, "Removing chatter: {0}", message);
          removeChatter(jsonMessage.getInt("id"));
          break;
        case "addMessage":
          LOG.log(Level.INFO, "Adding message: {0}", message);
          addMessage(new Message(
              messageCount++,
              jsonMessage.getString("text"),
              new Date(),
              chatters.get(jsonMessage.getInt("chatterId"))
          ));
          break;
        default:
          break;
      }
    }
  }
}
