var myChatterId;

window.onload = init;
var socket = new WebSocket("ws://localhost:8080/actions");
socket.onmessage = onMessage;

function onMessage(event) {
    var data = JSON.parse(event.data);
    if (data.action === "addMessage") {
        printMessage(data);
    }
    if (data.action === "addChatter") {
        printChatterElement(data);
    }
    if (data.action === "setChatterId") {
        myChatterId = data.id;
        printCurrentChatter(myChatterId);        
    }
    if (data.action === "removeChatter") {
        document.getElementById("chatter"+data.id).remove();
    }
}

function addMessage(text) {
    var MessageAction = {
        action: "addMessage",
        text: text,
        chatterId: myChatterId
    };
    socket.send(JSON.stringify(MessageAction));
}

function addChatter(nickName) {
    var ChatterAction = {
        action: "addChatter",
        nickName: nickName,
    };
    socket.send(JSON.stringify(ChatterAction));
}

function removeChatter() {
    var ChatterAction = {
        action: "removeChatter",
        id: myChatterId
    };
    socket.send(JSON.stringify(ChatterAction));
}
function printMessage(message) {
    var content = document.getElementById("messages");
    content.value = content.value + "(" + message.timestamp +","+message.chatterNickName+") " + message.text + "\n"
    content.scrollTop = content.scrollHeight;
}

function printCurrentChatter(chatterId) {
    var eChatter = document.getElementById("chatter"+chatterId);
    eChatter.setAttribute("class", "currentChatter");

    var logout = document.createElement("button");
    logout.setAttribute("id", "logout"+chatterId);
    logout.setAttribute("class", "button");
    logout.innerHTML = "Logout";
    eChatter.appendChild(logout);
    var eChatterForm = document.getElementById("addChatterForm");
    var eMessageForm = document.getElementById("addMessageForm");
    eChatterForm.hidden = true;
    eMessageForm.hidden = false;
    
    logout.onclick = function() {
        eChatterForm.hidden = false;
        eMessageForm.hidden = true;
        removeChatter();
    };
}

function printChatterElement(chatter) {
    var content = document.getElementById("chatters");
    
    var chatterDiv = document.createElement("div");
    chatterDiv.setAttribute("id", "chatter"+chatter.id);
    content.appendChild(chatterDiv);

    var chatterNickName = document.createElement("span");
    chatterNickName.setAttribute("class", "chatterNick");
    chatterNickName.innerHTML = chatter.nickName;
    chatterDiv.appendChild(chatterNickName);

}

function addMessageSubmit() {
    var form = document.getElementById("addMessageForm");
    var text = form.elements["message_text"].value;
    document.getElementById("addMessageForm").reset();
    addMessage(text);
}

function loginChatterSubmit() {
    var form = document.getElementById("addChatterForm");
    var nickName = form.elements["chatter_nickname"].value;
    document.getElementById("addChatterForm").reset();
    addChatter(nickName);
}
function init() {
    console.log("Initializing EAR Chat application");
    console.log(document.getElementById("chatters").childNodes);
    myChatterId = undefined;
    document.getElementById("chatters").innerHTML="";
    console.log(document.getElementById("chatters").childNodes);
    document.getElementById("messages").childNodes=[];
}