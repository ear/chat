# Sample Chat application using WebSockets

1. Run using `mvn spring-boot:run`
2. Reach out to `http://localhost:8080` from two different clients, adding chatter and sending messages in each of them.